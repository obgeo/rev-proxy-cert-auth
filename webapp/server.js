'use strict';

const express = require('express');
const path = require('path');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.get('/', (req, res) => {
  console.log('Request');
  res.render('index', {
    request: req,
    name: 'our template',
    link: 'https://google.com'
  });
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

function errorLogger(error, req, res, next) { // for logging errors
  console.error(error) // or using any fancy logging library
  next(error) // forward to next middleware
}

app.use(errorLogger);